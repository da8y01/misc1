// DancingPairs76.js

function findMaximumPairs(N, preferences) {
    let copyPreferences = preferences.slice()
    let formedPairs = []
    for (let index = 0; index < N; index++) {
        for (let indexP = 0; indexP < copyPreferences[index].length; indexP++) {
            for (let indexLady = 0; indexLady < array.length; indexLady++) {
                // [indexLady+1, copyPreferences[index][indexP]]
                formedPairs.push([indexLady+1, copyPreferences[index][indexP]])
                copyPreferences = updatePreferences(copyPreferences[index][indexP], copyPreferences)
            }
        }
        pairs.push(outter)
    }
    pairs.map((element, index) => {
        console.log(`Lady ${index+1} , generated ${element.length} lists:`)
        element.map((elm, idx) => {
            console.log(elm)
        })
    })
    return 0
}

function findMaximumPairs0(N, preferences) {
    const gentlemenPreferences = [];
    const paired = new Array(2 * N + 1).fill(false);
    for (let i = 0; i < N; i++) {
        const gentlemenIds = preferences[i];
        const ladyId = i + 1;
        gentlemenPreferences[ladyId] = gentlemenIds;
    }
    let pairs = 0;
    for (let ladyId = 1; ladyId <= N; ladyId++) {
        if (findPartner(ladyId, gentlemenPreferences, paired)) {
            pairs++;
        }
    }
    return pairs;
}

function findMaximumPairs1(N, preferences) {
    pairs = []
    for (let index = 0; index < N; index++) {
        for (let indexP = 0; indexP < preferences.length; indexP++) {
            for (let indexPrefOfLady = 0; indexPrefOfLady < preferences[indexP].length; indexPrefOfLady++) {
                if (indexP === index) {
                    pairs.push([index+1, preferences[indexP][indexPrefOfLady]])
                }
            }
            
        }
    }
    return pairs.length
}

function findPartner(ladyId, preferences, paired) {
    for (let index = 1; index <= N; index++) {
        const element = array[index];
    }
    for (const gentlemanId of gentlemenPreferences[ladyId]) {
        if (!paired[gentlemanId]) {
            paired[gentlemanId] = true;
            return true;
        }
    }
    return false;
}

function initFillWall(wall) {
    return wall.map((wallLady, idxLady) => {
        return wallLady.map((columnWallLady) => {
            return columnWallLady.map((pairOfColumnWallLady) => {
                pairOfColumnWallLady[0] = idxLady+1
                return pairOfColumnWallLady
            })
        })
    })
}

function initFillWall2(wall) {
    let wallCopy = wall.slice()
    for (let idxLady = 0; idxLady < wallCopy.length; idxLady++) {
        for (let idxColumn = 0; idxColumn < wallCopy[idxLady].length; idxColumn++) {
            for (let idxPair = 0; idxPair < wallCopy[idxLady][idxColumn].length; idxPair++) {
                // wallCopy[idxLady][idxColumn][idxPair][0] = idxLady
                wallCopy[idxLady][idxColumn][idxPair].splice(0, 1, idxLady)
            }
        }
    }
    return wallCopy
}

function printWall(wall) {
    console.log("THE LADY'S WALL")
    wall.map((e, i) => {
        console.log(`Wall #${i+1} has ${e.length} columns (because of the lady preferences):`)
        console.log(e)
    })
}

function iterateEachColumnOption(wall, preferences) {
    let copyPreferences = preferences.slice()
    let copyWall = wall.slice()
    for (let indexWallLady = 0; indexWallLady < copyWall.length; indexWallLady++) {
        for (let indexColumnWallLady = 0; indexColumnWallLady < copyWall[indexWallLady].length; indexColumnWallLady++) {
            let usedGentlements = []
            copyWall[indexWallLady][indexColumnWallLady][indexWallLady].splice(1, 1, copyPreferences[indexWallLady][indexColumnWallLady])
            let updatedPreferences = updatePreferences(copyPreferences[indexWallLady][indexColumnWallLady], copyPreferences)
            for (let indexUpdatedPreferences = 0; indexUpdatedPreferences < updatedPreferences.length; indexUpdatedPreferences++) {
                if ((copyWall[indexWallLady][indexColumnWallLady][indexUpdatedPreferences][1] === 0) && (usedGentlements.indexOf(updatedPreferences[indexUpdatedPreferences][0]) === -1)) {
                    let gentleman = updatedPreferences[indexUpdatedPreferences][0] || 0
                    copyWall[indexWallLady][indexColumnWallLady][indexUpdatedPreferences].splice(1, 1, gentleman)
                    usedGentlements.push(gentleman)
                }
            }
        }
        
    }
    printWall(copyWall)
    console.log(countWall(copyWall))
}

function countWall(wall) {
    let arrPairs = []
    for (let indexWall = 0; indexWall < wall.length; indexWall++) {
        for (let indexColumn = 0; indexColumn < wall[indexWall].length; indexColumn++) {
            let countPairs = 0
            for (let indexPair = 0; indexPair < wall[indexWall][indexColumn].length; indexPair++) {
                if (wall[indexWall][indexColumn][indexPair][1] !== 0) countPairs++
            }
            arrPairs.push(countPairs)
        }
    }
    return arrPairs
}

function fillWallStructure(wall) {
    return wall.map((wallLady, idxLady) => {
        return wallLady.map((columnWallLady) => {
            return columnWallLady.map((pairOfColumnWallLady, idxPairOfColumnWallLady) => {
                // pairOfColumnWallLady[0] = idxLady+1
                return pairOfColumnWallLady.toSpliced(0, 1, idxPairOfColumnWallLady+1)
            })
        })
    })
}

function initWallStructure(N, preferences) {
    const pair = new Array(2).fill(0)
    const column = new Array(N).fill(pair)
    const wall = new Array(N).fill(column)

    for (let index = 0; index < N; index++) {
        let wallLady = new Array(preferences[index].length).fill(column)
        wall[index] = wallLady
    }

    return wall

    // const wallFilled = fillWallStructure(wall)
    // iterateEachColumnOption(wallFilled, preferences)
}

function updatePreferences(gentlemanId, arrPreferences) {
    let arrResult = arrPreferences.map((elm, idx) => {
        let idxGentleman = elm.indexOf(gentlemanId)
        if (idxGentleman === -1) return elm
        else {
            let spliced = elm.toSpliced(idxGentleman, 1)
            return spliced
        }
    })
    return arrResult
}

function parsePreferences(arrPreferences) {
    return arrPreferences.map(elm => (elm.slice(elm.indexOf(":")+2).split(" ")));
    // return arrPreferences.map(elm => elm.slice(elm.indexOf(":")+2).split(" ").map(e => parseInt(e)))
}

process.stdin.on("data", data => {
    const splitted = data.toString().split("\n")
    const N = parseInt(splitted[0])
    console.log("N", N)
    const preferences = parsePreferences(splitted.slice(1))
    console.log("preferences", preferences)
    // const maximumPairs = findMaximumPairs(N, preferences)
    const maximumPairs = 0
    let emptyWall = initWallStructure(N, preferences)
    let filledWall = fillWallStructure(emptyWall)
    iterateEachColumnOption(filledWall, preferences)
    console.log(maximumPairs)
    return maximumPairs
})

// cat DancingPairs19.dat | node ./DancingPairs76.js
// 0
