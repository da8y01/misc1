# ca382-DieThrowSequences-v15.py

from random import randint


inN = 3
#target1 = 5151
target1 = 515
target1_lst = [int(digit) for digit in str(target1)]
target2 = 22122
target2_lst = [int(digit) for digit in str(target2)]
target3 = 1551551
target3_lst = [int(digit) for digit in str(target3)]
formed_sequence = []


def checkLastElements(sequence):
    if (len(sequence) >= len(target1_lst)):
        for idxTarget in range(len(target1_lst)):
            curr_idx = -(idxTarget + 1)
            if sequence[curr_idx] != target1_lst[curr_idx]:
                return False
        return True
    else:
        return False

def checkLastElements2(builded, target):
    builded.append(randint(1, 6))
    enter_while = True
    while enter_while:
        if len(builded) >= len(target):
            for idxTarget in range(len(target1_lst)):
                curr_idx = -(idxTarget + 1)
                if builded[curr_idx] == target1_lst[curr_idx]:
                    enter_while = True
                else:
                    break
                enter_while = False
        builded.append(randint(1, 6))

def buildSequence(formed):
    formed.append(randint(1, 6))
    if checkLastElements(formed):
        return formed
    else:
        return buildSequence(formed)


formed_sequence.append(buildSequence([]))
print(formed_sequence)
print(len(formed_sequence[0]))

# python ca382-DieThrowSequences-v15.py
# 88
