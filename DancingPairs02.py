# DancingPairs02.py

import fileinput

def find_max_pairs(N, preferences):
    gentlemen_preferences = {}

    # Initialize the gentlemen's preferences
    for i in range(N + 1, 2 * N + 1):
        gentlemen_preferences[i] = []

    # Populate the gentlemen's preferences
    for lady, partners in preferences.items():
        for partner in partners:
            gentlemen_preferences[partner].append(lady)

    # Perform the greedy pairing
    pairs = 0
    paired_ladies = set()
    for gentleman in range(N + 1, 2 * N + 1):
        for lady in gentlemen_preferences[gentleman]:
            if lady not in paired_ladies:
                paired_ladies.add(lady)
                pairs += 1
                break

    return pairs


if __name__ == "__main__":
    inN = 0
    preferences_input = {}

    count = 0
    for line in fileinput.input():
        count = count + 1
        if count == 1:
            inN = int(line)
            continue
        preferences_input[count-1] = [int(preferenceId) for preferenceId in line[line.index(":")+2:].split()]

    result = find_max_pairs(inN, preferences_input)
    print(result)

# cat DancingPairs19.dat | python ./DancingPairs02.py
# 16
