# Technical Challenges (2023-11)

from [https://docs.autonomicjump.com/templates-and-tips/builds](https://docs.autonomicjump.com/templates-and-tips/builds)

## Install Nix

## Install fa Makes
[https://github.com/fluidattacks/makes/blob/main/makes/main.nix](https://github.com/fluidattacks/makes/blob/main/makes/main.nix "makes/main.nix")

## Meet the builder
Run `m .`
![run builds 1](./BuildM1.png "run builds 1")
![run builds 2](./BuildM2.png "run builds 2")

## Prepare commit
![Prepare commit 1](./Commit1.png "Prepare commit 1")

## Exercises attempted

### dancing-pairs 202
[https://www.codeabbey.com/index/task_view/dancing-pairs](https://www.codeabbey.com/index/task_view/dancing-pairs "dancing-pairs 202")

[[NodeJS](./DancingPairs76.js "DancingPairs76.js")]
![Attempt202_NodeJS](./Attempt202_NodeJS.png "Attempt202_NodeJS")

[[Python](./DancingPairs02.py "DancingPairs02.py")]
![Attempt202_Python](./Attempt202_Python.png "Attempt202_Python")

### die-throw-sequences 382
[https://www.codeabbey.com/index/task_view/die-throw-sequences](https://www.codeabbey.com/index/task_view/die-throw-sequences "die-throw-sequences 382")

[[Python](./ca382-DieThrowSequences-v15.py "ca382-DieThrowSequences-v15.py")]
![Attempt382_Python](./Attempt382_Python.png "Attempt382_Python")

[Python OnLine]
![Attempt382_Python_OnLine](./Attempt382_Python_OnLine.png "Attempt382_Python_OnLine")